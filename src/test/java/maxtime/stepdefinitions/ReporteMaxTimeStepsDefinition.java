package maxtime.stepdefinitions;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import maxtime.tasks.Abrir;
import maxtime.tasks.Ingresar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class ReporteMaxTimeStepsDefinition {
	
	@Managed(driver="chrome")
	private WebDriver navegadorChrome;
	private Actor Aleja = Actor.named("Alejandra");
	
	@Before
	public void configuracionInicial() {
		Aleja.can(BrowseTheWeb.with(navegadorChrome));
	}
	
	@Given("^que soy una analista de pruebas$")
	public void queSoyUnaAnalistaDePruebas() {
		Aleja.wasAbleTo(Abrir.laPaginaDeMaxTime());
	}

	@When("^ingreso a realizar el reporte de mis actividades en MaxTime$")
	public void ingresoARealizarElReporteDeMisActividadesEnMaxTime() {
		Aleja.attemptsTo(Ingresar.alRegistroDeReportes());
	}

	@Then("^debe quedar el dia reportado$")
	public void debeQuedarElDiaReportado() {
	}


}

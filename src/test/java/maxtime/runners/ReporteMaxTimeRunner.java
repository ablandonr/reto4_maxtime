package maxtime.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (
		features = "src/test/resourses/ReporteMaxTime.feature",
		tags= "@ReporteExitoso",
		glue="maxtime.stepdefinitions", 
		snippets= SnippetType.CAMELCASE)

public class ReporteMaxTimeRunner {
	

}
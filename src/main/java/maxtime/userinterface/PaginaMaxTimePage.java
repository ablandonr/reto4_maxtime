package maxtime.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxTimeCHC/Login.aspx")
public class PaginaMaxTimePage extends PageObject{
	public final Target CAMPO_NOMBRE = Target.the("Campo para ingresar el nombre").located(By.id("Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I"));
	public final Target CAMPO_CLAVE = Target.the("Campo para ingresar la clave").located(By.id("Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I"));
	public final Target BOTON_INGRESAR = Target.the("Click en el boton Ingresar").located(By.id("Logon_PopupActions_Menu_DXI0_T"));
	public final Target BOTON_PRIMER_REGISTRO = Target.the("Click en el primer enlace").located(By.id("Vertical_v1_LE_v2_tccell0_4"));
	public final Target BOTON_NUEVO = Target.the("Click en el boton de nuevo").located(By.id("Vertical_SHC_Menu_DXI0_P"));
	public final Target BOTON_EDITAR = Target.the("CLick en el boton de editar").located(By.id("Vertical_SHC_Menu_DXI0i0_T"));
	public final Target BOTON_CANCELAR = Target.the("Click en el boton de cancelar").located(By.id("Vertical_EMA_Menu_DXI3_T"));
	public final Target BOTON_NUEVO_REGISTRO = Target.the("Click en el boton de nuevo reporte").located(By.id("Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_ToolBar_Menu_DXI0_T"));
	public final Target BOTON_PROYECTO = Target.the("Click para ingresar el proyecto").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l128_xaf_dviProyecto_Edit_Find_BImg"));
	public final Target CAMPO_BUSCAR = Target.the("Ingresar el proyecto").located(By.id("Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_CD"));
	public final Target BOTON_BUSCAR = Target.the("Click en buscar").located(By.id("Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_CD"));
	public final Target BOTON_PROYECTO_PITT  = Target.the("Click en el proyecto").located(By.id("Dialog_v7_LE_v8_DXMainTable"));
}

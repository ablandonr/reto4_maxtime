package maxtime.tasks;

import maxtime.userinterface.PaginaMaxTimePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	PaginaMaxTimePage paginaMaxTimePage;
	@Override
	public <T extends Actor> void performAs(T Aleja) {
		Aleja.attemptsTo(Open.browserOn(paginaMaxTimePage));
	}

	public static Abrir laPaginaDeMaxTime() {
		return Tasks.instrumented(Abrir.class);
	}

}

package maxtime.tasks;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import maxtime.userinterface.PaginaMaxTimePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Ingresar implements Task{

	PaginaMaxTimePage paginaMaxTimePage;
	List<WebElement> lista;
	
	@Override
	public <T extends Actor> void performAs(T Aleja) {
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.CAMPO_NOMBRE));
		Aleja.attemptsTo(Enter.theValue("ablandonr").into(paginaMaxTimePage.CAMPO_NOMBRE));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.CAMPO_CLAVE));
		Aleja.attemptsTo(Enter.theValue("Mede789*").into(paginaMaxTimePage.CAMPO_CLAVE));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_INGRESAR));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_PRIMER_REGISTRO));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_NUEVO));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_EDITAR));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_CANCELAR));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_NUEVO_REGISTRO));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_PROYECTO));
		Aleja.attemptsTo(Enter.theValue("PITT").into(paginaMaxTimePage.CAMPO_BUSCAR));
		Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_BUSCAR));
		//Aleja.attemptsTo(Click.on(paginaMaxTimePage.BOTON_PROYECTO_PITT));
		/*lista = paginaMaxTimePage.BOTON_PROYECTO_PITT.resolveFor(Aleja).findElements(By.tagName("tr"));
		for( int i=0; i<lista.size();i++) {
			System.out.println(lista.get(i).getText());
			if(lista.get(i).getText().contains("Dialog_v7_LE_v8_DXHeadersRow0")) {
				lista.get(i).click();
				break;
			}
		}*/
	}

	public static Ingresar alRegistroDeReportes() {
		return Tasks.instrumented(Ingresar.class);
	}

}
